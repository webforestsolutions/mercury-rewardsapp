export interface Environment {
  mode: string;
  url: string;
}
