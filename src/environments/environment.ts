import { Environment } from './environment.model';

const URL = {
    live: 'http://172.104.60.122/',
    staging: 'http://172.104.181.122/',
    localhost: 'http://localhost:8000/',
    localbuild: 'http://192.168.254.105/webforest/mercury-api/public/' // replace with IP address of your local machine
}

export const ENV: Environment = {
  mode: 'Production',
    url: URL.localhost
}
