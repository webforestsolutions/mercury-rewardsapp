export class Pricing {

    constructor() {
        this.name = '';
        this.code = '';
        this.type = '';
        this.discount = '';
        this.apply_to = '';
    }

    id?: number;
    name: string;
    code: string;
    status?: string;
    type: string;
    discount: string;
    apply_to: string;
    size;
    metrics: string;
    product_id;
    product_variation_id;
    product_name: string;
}