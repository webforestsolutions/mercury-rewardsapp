export class BranchSaleSummary {

    constructor() {

    }

    name:string;
    sales:number;
    cost_of_sales:number;
    returns:number;
    discounts:number;
    net_sales:number;
    gross_profit:number;
    gross_profit_margin:number;

}