export class SingleAdjustment {

    constructor(){
        this.type = "";
        this.remarks = "";
        this.remarks_code = "";
        this.product_variation_id = null;
        this.shortover_product_variation_id = null;
        this.or_no = "";
    }

    staff_id: number;
    type:string;
    remarks: string;
    remarks_code: string;
    product_variation_id: number;
    shortover_product_variation_id: number;
    quantity: number;
    or_no: string;

    getRemarksByCode(remarksCode: string){

        let remarks = "";

        switch(remarksCode){
            case 'short':
                remarks = "Short";
                break;
            case 'lost':
                remarks = "Lost";
                break;
            case 'damage':
                remarks = "Damage";
                break;
            case 'shortover':
                remarks = "Short/Over";
                break;
        }

        return remarks;

    }


}