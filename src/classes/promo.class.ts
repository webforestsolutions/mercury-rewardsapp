export class Promo {

    name: string;
    description: string;
    code: string;
    image: string;
    start_date: string;
    end_date: string;

    // getDuration(): string {

    //     const start = this.start_date;
    //     const end = this.end_date;

    //     return 'PROMO VALID FROM {$start} TO {$end}';
    // }

}
