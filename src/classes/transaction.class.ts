import {ProductVariation} from "./product-variation.class";
export class Transaction {


    constructor() {

        this.isViewed = false;
        this.items = [];
    }

    id?: number;

    key:string;

    or_no: string;
    customer_id: string;
    transaction_type: string;
    transaction_type_name: string;

    transaction_type_id: number;
    staff_id: number;
    customer_user_id: number;
    customer_firstname: string;
    customer_lastname: string;
    customer_phone: string;
    staff_firstname: string;
    staff_lastname: string;
    price_rule_id: number;
    price_rule_code: string;
    discount: number = 0;

    user_id: number;
    branch_id: number;

    created_at: string;

    status: string;

    isViewed: boolean;
    items: ProductVariation[] = [];
    returns: ProductVariation[] = [];
    items_string: string;
    shortover_string: string;

    total:number = 0;

    has_returns: number = 0;
    remarks: string;

    vat_price: number = 0;
    vat_percentage: number = 0;
    subtotal:number = 0;

    setEmptyItems() {
        this.items = [];
    }

    isEmpty() {

        if (typeof this.items == null) {
            this.items = [];
        }

        if (this.items.length > 0) {
            return false;
        }

        return true;

    }

    addToCart(item: ProductVariation){

        if(item.isSelected){
            return false;
        }

        item.isSelected = true;

        item.quantity = 1;
        this.items.push(item);
        this.computeTotal();
    }

    removeCartItem(index: number){

        this.items.splice(index,1);
        this.computeTotal();

    }

    addQuantity(item: ProductVariation){

        const itemAdded = item.addQuantity();

        if(!itemAdded){
            return false;
        }

        this.computeTotal();

        return true;

    }

    removeQuantity(item: ProductVariation, force:boolean = false){

        const itemRemoved = item.removeQuantity(force);

        if(!itemRemoved){
            return false;
        }

        this.computeTotal();

        return true;

    }

    computeTotal() {

        this.total = 0;

        this.items.forEach((item) => {
            this.total += parseFloat(''+item.selling_price)*parseFloat(''+item.quantity);
        });

        if(isNaN(this.total)){
            return 0;
        }

        this.total.toFixed(2);

        return this.total;
    }

    calculateSubtotalMinusVat() {

        const subTotalMinusVat = parseFloat(''+this.total) - parseFloat(''+this.vat_price);

        if(isNaN(subTotalMinusVat)){
            return 0;
        }

        this.subtotal = subTotalMinusVat;

    }

    calculateVatPrice(vat: number){

        const vatPrice = parseFloat(''+this.total)*parseFloat(''+vat);

        if(isNaN(vatPrice)){
            return 0;
        }

        this.vat_price = vatPrice;

    }

    calculateVatPercentage(vat: number) {

        const vatPercentage = parseFloat(''+vat)*100;

        if(isNaN(vatPercentage)){
            return 0;
        }

        this.vat_percentage = vatPercentage;

    }

    setItem(productVariationId:number, quantity:number) {

        this.setEmptyItems();

        const productVariation = new ProductVariation();

        productVariation.product_variation_id = productVariationId;
        productVariation.quantity = quantity;

        this.items.push(productVariation);

    }

    setShortOverItem(productVariationId:number, shortOverProductVariationId:number, quantity:number) {

        this.setEmptyItems();

        const productVariation = new ProductVariation();

        productVariation.product_variation_id = productVariationId;
        productVariation.shortover_product_variation_id = shortOverProductVariationId;
        productVariation.quantity = quantity;

        this.items.push(productVariation);

    }
}