export class Page {

    constructor() {
        this.title = "";
    }

    private key?:string;
    private title: string;
    private component: any;
    private role?: any;

    setTitle(title:string){
        this.title = title;
    }

    setComponent(component: any) {
        this.component = component;
    }

    setKey(key:string){
        this.key = key;
    }

    setRole(role: any) {
        this.role = role;
    }

}