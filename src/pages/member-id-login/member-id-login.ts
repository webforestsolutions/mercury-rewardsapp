import {Component, OnDestroy, OnInit} from '@angular/core';
import {AlertController, MenuController, NavController} from 'ionic-angular';
import {ErrorResponseService} from "../../shared/services/helpers/error-response.service";
import {ErrorResponse} from "../../classes/error-response.class";
import {TrackingEventService} from "../../shared/services/helpers/tracking-event.service";
import {StorageService} from "../../shared/services/helpers/storage.service";
import {RewardService} from "../../shared/services/api/reward.service";
import {Constants} from "../../shared/constants";


@Component({
    selector: 'page-member-id-login',
    templateUrl: 'member-id-login.html'
})

export class MemberIdLoginPage implements OnInit, OnDestroy {

    memberId: string = '';

    currentStep = 1;
    height = window.innerHeight;

    alertCode: string = '';
    alertTitle: string = '';
    alertMessage: string = '';

    errorResponse: ErrorResponse = new ErrorResponse();

    constructor(public navCtrl: NavController,
                private errorResponseService: ErrorResponseService,
                private trackingEventService: TrackingEventService,
                private menu: MenuController,
                private alert: AlertController,
                private storageService: StorageService,
                private rewardService: RewardService) {
        this.menu.enable(false);
    }

    ngOnInit() {
        this.trackingEventService.dismissLoading();
    }

    ngOnDestroy() {

    }

    verifyMemberById(memberId: string) {

        this.trackingEventService.loading('Verifying Membership...');

        this.rewardService.getRewardsByMemberId(memberId).toPromise()
            .then(
                (member) => {
                    console.log('MEMBER RESPONSE: ', member);
                    return this.storageService.set(Constants.memberId, member.customer_id)
                }
            )
            .then(
                (memberId) => {
                    return this.trackingEventService.onMemberVerified.emit(memberId);
                }
            )
            .then(
                () => {
                    return this.trackingEventService.onLogin.emit(true);
                }
            )
            .then(
                () => {
                    return this.trackingEventService.dismissLoading();
                }
            )
            .catch(
                (error) => {

                    console.log('ERROR -', error);

                    this.trackingEventService.dismissLoading();

                    if (error.status == 0) {
                        this.alertCode = 'err_connection_refused';
                    }

                    if (error.status == 404) {
                        this.alertCode = 'user_not_found';
                    }

                    this.showAlert();

                    // this.errorResponse = this.errorResponseService.handleError(error);
                }
            );
    }

    // ON-TAP FUNCTIONS

    onLogin() {

        this.verifyMemberById(this.memberId);

    }

    onCancel() {

        this.navCtrl.popToRoot();

    }

    // ALERT

    showAlert(): void {

        if (typeof this.alertCode == 'undefined') {
            return;
        }

        const code = this.alertCode;

        switch(code) {
            case 'user_not_found':
                this.alertTitle = 'User Not Found';
                this.alertMessage = 'Please make sure that your ID is correct';
                break;

            case 'err_connection_refused':
                this.alertTitle = 'Error';
                this.alertMessage = 'Could not connect to the server. Please check if you are connected to the internet';
                break;

            default:
                this.alertTitle = 'Error';
                this.alertMessage = 'Please ask for help from the nearest Augustus branch';
                break;
        }

        let alert = this.alert.create({
            title: this.alertTitle,
            message: this.alertMessage,
            buttons: [
                {
                    text: 'Okay'
                }
            ]
        });

        alert.present()
            .then(
                () => {
                    this.resetAlertData();
                }
            )
            .catch(
                (error) => {
                    console.log('Something went wrong while trying show the alert modal - ', error);
                }
            );

    }

    private resetAlertData(): void {

        this.alertCode = '';
        this.alertTitle = '';
        this.alertMessage = '';
    }

}
