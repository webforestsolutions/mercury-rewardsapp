import {Component, OnDestroy, OnInit} from '@angular/core';

import {
    AlertController, MenuController, NavController, ViewController, Platform
} from 'ionic-angular';

import {
    RewardService, StorageService, TrackingEventService, UserService
} from '../../shared/services';

import {User} from "../../classes/user.class";

@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})
export class HomePage implements OnInit {

    page: string = 'account';

    height = window.innerHeight;

    title: string = 'Home Page';
    description: string = 'Welcome to Augustus Rewards';

    currentMember: any = new User();

    constructor(public navCtrl: NavController,
                public platform: Platform,
                public viewCtrl: ViewController,
                private alertCtrl: AlertController,
                private trackingEventService: TrackingEventService,
                private menu: MenuController,
                private userService: UserService,
                private rewardService: RewardService) {}

    ngOnInit(): void {

        this.menu.enable(false);

        this.getCurrentUser();

    }

    ionViewDidEnter() {
        console.log('Navigated to HOME/ACCOUNT PAGE');

        this.trackingEventService.onSwitchPage.emit(this.page);

        const platform = this.platform.ready();

        platform.then(() => {
             this.platform.registerBackButtonAction(() => {

                if (this.viewCtrl.enableBack()) {
                    return this.navCtrl.pop();
                }

                return this.onLogout();

            }, 0);
        }).catch((error) => {
            console.log('Something went wrong with the platform - ', error);
        });

    }

    private onLogout(): void {

        let alert = this.alertCtrl.create({
            title: 'Confirm Logout',
            message: 'Are you sure you want to logout?',
            buttons: [
                {
                    text: 'No',
                    role: 'Cancel',
                    cssClass: 'cancel',
                    handler: () => {
                        console.log('Clicked cancel');
                    }
                },
                {
                    text: 'Yes',
                    cssClass: 'confirm',
                    handler: () => {
                        this.trackingEventService.onLogin.emit(false);
                        this.trackingEventService.onLogout.emit('logout');
                    }
                }
            ]
        });

        alert.present().then(
            () => {
                // DO SOMETHING HERE
            }
        )
        .catch(
            (error) => {
                console.log('Something went wrong while presenting the alert');
            }
        );
    }

    private getCurrentUser(): void {
        this.userService.getCurrentCustomerId()
            .then((id) => {
                return this.getMemberByCustomerId(id);
            })
            .catch((error) => {
                console.log('Something went wrong while retrieving the customer ID - ', error);
            });

    }

    private getMemberByCustomerId(id: string): Promise<any> {
        return this.rewardService.getRewardsByMemberId(id).toPromise()
            .then((member) => {
                    return this.currentMember =  member;
                }
            )
            .catch((error) => {
                console.log('Something went wrong while retrieving the member\'s information - ', error);
            });
    }
}
