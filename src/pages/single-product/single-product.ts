import {ChangeDetectorRef, Component, OnDestroy, OnInit} from "@angular/core";

import {
    NavParams, NavController, Platform
} from "ionic-angular";

// SERVICES
import {
    TrackingEventService,
    StorageService
} from '../../shared/services';

@Component({
    selector: "page-single-product",
    templateUrl: "single-product.html"
})
export class SingleProductPage implements OnInit {

    product: any = {
        name: '',
        description: '',
        image: '',
        size: null,
        metrics: ''
    };

    constructor(public navParams: NavParams, public navCtrl: NavController, public platform: Platform) {}

    ngOnInit(): void {

        this.setProductDetails();

        this.platform.registerBackButtonAction(() => {
            this.navCtrl.pop();
        }, 0);
    }

    private setProductDetails(): void {
        this.product = this.navParams.get('product');
    }

}
