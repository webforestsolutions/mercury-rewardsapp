import {ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';

import {
    NavController, ViewController, Platform
} from 'ionic-angular';

// SERVICES
import {
    // PromoService,
    TrackingEventService
} from '../../shared/services';

// PAGES
import {
    HomePage
} from '../../pages';

// CLASSES
import {
    Promo
} from '../../classes';

@Component({
    selector: 'page-promos',
    templateUrl: 'promos.html'
})
export class PromosPage implements OnInit {

    page: string = 'promo';

    defaultPromoImage: string = 'assets/promos/promo-30ml.jpg';

    promos: Promo[] = [
        {
            name: 'Test Promo 01',
            description: 'Just a static promo',
            code: 'test-promo-01',
            image: 'Test Promo Image Here',
            start_date: 'Mar 21, 2018',
            end_date: 'Apr 21, 2018',
        }
    ];

    constructor(
        public navCtrl: NavController,
        public viewCtrl: ViewController,
        public platform: Platform,
        private trackingEventService: TrackingEventService) {}

    ngOnInit(): void {
        // this.getAllPromos();
    }

    ionViewDidEnter(): void {

        console.log('Navigated to Promos page');

        this.trackingEventService.onSwitchPage.emit(this.page);

        const platform = this.platform.ready();

        platform.then(() => {
             this.platform.registerBackButtonAction(() => {

                if (this.viewCtrl.enableBack()) {
                    return this.navCtrl.pop();
                }

                return this.navCtrl.push(HomePage);

            }, 0);
        }).catch((error) => {
            console.log('Something went wrong with the platform - ', error);
        });

    }

    private getAllPromos(): void {

        // this.promoService.getAll()
        //     .then((response) => {
        //             this.promos = response;
        //         }
        //     )
        //     .catch((error) => {
        //             console.log('Something went wrong while retrieving promos - ', error);
        //         }
        //     );

    }

}
