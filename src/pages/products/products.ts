import {ChangeDetectorRef, Component, OnDestroy, OnInit} from "@angular/core";

import {NavController, ViewController, Platform} from "ionic-angular";

// PAGES
import {
    SingleProductPage,
    HomePage
} from "../../pages";

// SERVICES
import {
    TrackingEventService
} from '../../shared/services';

@Component({
    selector: "page-products",
    templateUrl: "products.html"
})
export class ProductsPage implements OnInit {

    page: string = 'products';

    // LIST OF PERFUMES
    perfumes: any = {

        men: [
            {
                name: 'Mandarin',
                description: 'You can wear this for all seasons. It smells sensual and classy.',
                image: 'assets/perfumes/mandarin.png',
                size: 60,
                metrics: 'ml'
            },
            {
                name: 'Musk',
                description: 'The aromatic smell is flexible you can wear to the gym or running errands. Cinnamon adds warmth and there is still a masculine vibe to the scent.',
                image: 'assets/perfumes/musk.png',
                size: 60,
                metrics: 'ml'
            },
            {
                name: 'Tonka',
                description: 'This fragrance is really masculine, sexy, sophisticated and sometimes elegant.',
                image: 'assets/perfumes/tonka.png',
                size: 60,
                metrics: 'ml'
            },
            {
                name: 'Green Tea',
                description: 'So refreshing and sexy you can wear it day and night. If you wanna feel sexy and fresh wear it night and day.',
                image: 'assets/perfumes/green-tea.png',
                size: 60,
                metrics: 'ml'
            },
            {
                name: 'Intense',
                description: 'Design for the man of today. Highly versatile, masculine and wearable anytime time of the day.',
                image: 'assets/perfumes/intense.png',
                size: 60,
                metrics: 'ml'
            },
            {
                name: 'Light',
                description: 'It’s a bit more refined and elegant. It opens up with a heavy presence of crisp and sweet aroma, and provides a very pure, clean, and classy feeling.',
                image: 'assets/perfumes/light.png',
                size: 60,
                metrics: 'ml'
            },
            {
                name: 'Mint',
                description: 'You will always get compliments wearing this fragrance because it is cool and comforting.',
                image: 'assets/perfumes/mint.png',
                size: 60,
                metrics: 'ml'
            },
            {
                name: 'Sandal Wood',
                description: 'Fresh and sharp, simple and masculine yet so ocean feel.',
                image: 'assets/perfumes/sandal-wood.png',
                size: 60,
                metrics: 'ml'
            },
            {
                name: 'Cedar',
                description: 'This is very simple, clean apple smelling scent. The smell is so addicting.',
                image: 'assets/perfumes/cedar.png',
                size: 60,
                metrics: 'ml'
            },
            {
                name: 'Sypress',
                description: 'You wanna get noticed? Wear sypress. Perfect for Asian men and smell gorgeous.',
                image: 'assets/perfumes/sypress.png',
                size: 60,
                metrics: 'ml'
            },
            {
                name: 'Spicy Berries',
                description: 'Nostalgic scents that will make you feel smart and active.',
                image: 'assets/perfumes/spicy-berries.png',
                size: 60,
                metrics: 'ml'
            },
            {
                name: 'Suede',
                description: 'The redeeming factor once it dries in your skin. Stand out among others. Wear Suede.',
                image: 'assets/perfumes/suede.png',
                size: 60,
                metrics: 'ml'
            },
            {
                name: 'Aqua',
                description: 'Very classy scent. It smells so nice and fresh with touch of uniqueness.',
                image: 'assets/perfumes/aqua.png',
                size: 60,
                metrics: 'ml'
            }
        ],

        women: [
            {
                name: 'Iris',
                description: 'Captures the purity of a woman. Almost deceiving simple yet wonderful and addictive.',
                image: 'assets/perfumes/iris.png',
                size: 60,
                metrics: 'ml'
            },
            {
                name: 'Lily',
                description: 'Sophisticated fragrance for every woman, perfect for everyday wear or that romantic evening out.',
                image: 'assets/perfumes/lily.png',
                size: 60,
                metrics: 'ml'
            },
            {
                name: 'Dewberry',
                description: 'Get in touch with your softer side. Experience flirty and romantic with sexy playfulness of fantasy.',
                image: 'assets/perfumes/dewberry.png',
                size: 60,
                metrics: 'ml'
            },
            {
                name: 'Freesia',
                description: 'Is lovely gentle light, very smooth and subtle.',
                image: 'assets/perfumes/freesia.png',
                size: 60,
                metrics: 'ml'
            },
            {
                name: 'Hyacinth',
                description: 'Is fragrance of joy, the essence of a sunny and happy morning. Wear happy and be happy.',
                image: 'assets/perfumes/hyacinth.png',
                size: 60,
                metrics: 'ml'
            },
            {
                name: 'Honey',
                description: 'Is rich bouquet of flowers after the rain, suited for every woman in every season.',
                image: 'assets/perfumes/honey.png',
                size: 60,
                metrics: 'ml'
            },
            {
                name: 'Sicilian',
                description: 'Is a casual and breezy, sparkling scent that evokes the spirit of the inner beauty.',
                image: 'assets/perfumes/sicilian.png',
                size: 60,
                metrics: 'ml'
            },
            {
                name: 'Jasmine',
                description: 'This is light, fresh and feminine. Perfect scent for all occasions.',
                image: 'assets/perfumes/jasmine.png',
                size: 60,
                metrics: 'ml'
            },
            {
                name: 'Chypre',
                description: 'Perfect for work or dinner a great everyday perfume that will stand out.',
                image: 'assets/perfumes/chypre.png',
                size: 60,
                metrics: 'ml'
            },
            {
                name: 'Bergamot',
                description: 'A young and, uncomplicated scent that’s great to wear when you want to lift your spirit.',
                image: 'assets/perfumes/bergamot.png',
                size: 60,
                metrics: 'ml'
            },
            {
                name: 'Peony',
                description: 'Love this fragrance. Very joyful, girly, energetic and summer like.',
                image: 'assets/perfumes/peony.png',
                size: 60,
                metrics: 'ml'
            },
            {
                name: 'Amber',
                description: 'Release your inner diva with this sultry sweet scent and oozing sexiness when your spray all over your body.',
                image: 'assets/perfumes/amber.png',
                size: 60,
                metrics: 'ml'
            },
            {
                name: 'Raspberry',
                description: 'Sweet simple and honest, very innocent and charming.',
                image: 'assets/perfumes/raspberry.png',
                size: 60,
                metrics: 'ml'
            }
        ]
    };

    selected: any = {
        men: true,
        women: false
    }

    constructor(
        public navCtrl: NavController,
        public viewCtrl: ViewController,
        public platform: Platform,
        private trackingEventService: TrackingEventService) {}

    ngOnInit(): void {

    }

    ionViewDidEnter() {

        console.log('Navigated to PRODUCTS PAGE');

        this.trackingEventService.onSwitchPage.emit(this.page);

        const platform = this.platform.ready();

        platform.then(() => {
             this.platform.registerBackButtonAction(() => {

                if (this.viewCtrl.enableBack()) {
                    return this.navCtrl.pop();
                }

                return this.navCtrl.push(HomePage);

            }, 0);
        }).catch((error) => {
            console.log('Something went wrong with the platform - ', error);
        });

    }

    // CLICK FUNCTIONS

    onSelectPerfumeType(type: string): void {

        switch(type) {
            case 'men':
                this.selected.men = true;
                this.selected.women = false;
                break;
            case 'women':
                this.selected.women = true;
                this.selected.men = false;
                break;
            default:
                this.selected.men = true;
                this.selected.women = false;
                break;
        }
    }

    onViewProduct(product): void {
        this.navCtrl.push(SingleProductPage, {product});
    }

}
