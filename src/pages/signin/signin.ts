import {Component, OnDestroy, OnInit} from '@angular/core';
import {AlertController, MenuController, NavController, Platform} from 'ionic-angular';
import {AuthService} from "../../shared/services/api/auth.service";
import {ErrorResponseService} from "../../shared/services/helpers/error-response.service";
import {ErrorResponse} from "../../classes/error-response.class";
import {TrackingEventService} from "../../shared/services/helpers/tracking-event.service";
import {StorageService} from "../../shared/services/helpers/storage.service";
import {RewardService} from "../../shared/services/api/reward.service";
import {BarcodeScanner, BarcodeScannerOptions} from "@ionic-native/barcode-scanner";
import {Constants} from "../../shared/constants";

// PAGES

import {
    MemberIdLoginPage
} from '../../pages';


@Component({
    selector: 'page-signin',
    templateUrl: 'signin.html'
})

export class SigninPage implements OnInit, OnDestroy {
    memberId: string;
    height = window.innerHeight;
    errorResponse: ErrorResponse = new ErrorResponse();

    // alert-related variables
    alertCode: string = '';
    alertTitle: string = '';
    alertMessage: string = '';
    alertShown: boolean = false;

    // variables for showing or hiding
    inputMemberId: boolean = false;

    constructor(
        public navCtrl: NavController,
        public platform: Platform,
        private authService: AuthService,
        private errorResponseService: ErrorResponseService,
        private trackingEventService: TrackingEventService,
        private menu: MenuController,
        private alert: AlertController,
        private storage: StorageService,
        private barcodeScanner: BarcodeScanner,
        private rewardService: RewardService
    ) {}

    ngOnInit() {
        this.menu.enable(false);
        this.trackingEventService.dismissLoading();

        this.platform.registerBackButtonAction(() => {
            console.log('CLICKED BACK BUTTON');
            if (!this.alertShown) {
                this.presentConfirm();
            }
        }, 0);
    }

    ngOnDestroy() {

    }

    presentConfirm() {
        let alert = this.alert.create({
            title: 'Confirm Exit',
            message: 'Close Augustus Rewards App?',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: () => {
                        console.log('Cancel clicked');
                        this.alertShown = false;
                    }
                },
                {
                    text: 'Yes',
                    handler: () => {
                        console.log('Yes clicked');
                        this.platform.exitApp();
                    }
                }
            ]
        });

        alert.present().then(()=>{
            this.alertShown = true;
        });
    }

    // ON-TAP FUNCTIONS

    verifyMemberId(memberId: string, textInput: boolean = false) {

        if (textInput) {
            this.trackingEventService.loading();
        }

        this.rewardService.getRewardsByMemberId(memberId).toPromise()
            .then(
                (member) => {
                    console.log('MEMBER RESPONSE: ', member);
                    return this.storage.set(Constants.memberId, member.customer_id)
                }
            )
            .then(
                (memberId) =>  {
                    this.trackingEventService.onMemberVerified.emit(memberId);
                    this.trackingEventService.onLogin.emit(true);
                    return this.trackingEventService.dismissLoading();
                }
            )
            .catch(
                (error) => {
                    console.log('Error verifying member id: ', error);

                    this.trackingEventService.dismissLoading();

                    if (error.status == 0) {
                        this.alertCode = 'err_connection_refused';
                    }

                    if (error.status == 404) {
                        this.alertCode = 'user_not_found';
                    }

                    this.showAlert();

                }
            );
    }

    onScanCard() {

        const options: BarcodeScannerOptions = {
            prompt: "Scan QR code within the box.",
            formats: "QR_CODE"
        };

        this.barcodeScanner.scan(options)
            .then(
                (barcodeData) => {

                    const memberId = barcodeData.text;

                    console.log('barcode data: ', barcodeData);

                    if (barcodeData.cancelled) {
                        console.log('BARCODE CANCELLED - ', barcodeData);
                        console.log('Cancelled');
                        return false;
                    }

                    return memberId;

                }
            )
            .then(
                (memberId: any) => {

                    if (!memberId) {
                        this.trackingEventService.dismissLoading();
                        return false;
                    }

                    this.trackingEventService.loading('Verifying Membership...');

                    this.memberId = memberId;

                    this.verifyMemberId(memberId);
                }
            )
            .catch(
                (error) => {
                    this.trackingEventService.dismissLoading();
                    console.log('Something went wrong after scanning the code: ', error);
                }
            );
    }

    // ALERT

    showAlert(): void {

        if (typeof this.alertCode == 'undefined') {
            return;
        }

        const code = this.alertCode;

        switch(code) {
            case 'user_not_found':
                this.alertTitle = 'User Not Found';
                this.alertMessage = 'Please make sure that your ID is correct';
                break;

            case 'err_connection_refused':
                this.alertTitle = 'Error';
                this.alertMessage = 'Could not connect to the server. Please make sure you are connected to the internet.';
                break;

            default:
                this.alertTitle = 'Error';
                this.alertMessage = 'Please ask for help from the nearest Augustus branch';
                break;
        }

        let alert = this.alert.create({
            title: this.alertTitle,
            message: this.alertMessage,
            buttons: [
                {
                    text: 'Okay'
                }
            ]
        });

        alert.present()
            .then(
                () => {
                    this.resetAlertData();
                }
            )
            .catch(
                (error) => {
                    console.log('Something went wrong while trying show the alert modal - ', error);
                }
            );
    }

    private resetAlertData(): void {
        this.alertCode = '';
        this.alertTitle = '';
        this.alertMessage = '';
    }

}
