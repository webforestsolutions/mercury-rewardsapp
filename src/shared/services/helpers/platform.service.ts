import {Injectable} from "@angular/core";
import "rxjs/Rx";

@Injectable()
export class PlatformService {

    width:string;
    height:string;

    constructor() {

    }

    setWidth(width:number){
        this.width = width+'px';
    }

    setHeight(height:number){
        this.height = height+'px';
    }

}