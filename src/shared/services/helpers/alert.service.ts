import {Injectable} from "@angular/core";
import "rxjs/Rx";
import {ToastrService} from "ngx-toastr";

@Injectable()
export class AlertService {

    constructor(private toastr: ToastrService) {

    }

    notifySuccess(message: string, title: string = "SUCCESS") {
        this.toastr.success(message, title);
    }

}