import {EventEmitter, Injectable} from "@angular/core";
import "rxjs/Rx";

import {ProductCategory} from "../../../classes";
import {LoadingController} from "ionic-angular";

@Injectable()
export class TrackingEventService {

    onMemberVerified: EventEmitter<number> = new EventEmitter<number>();

    onLogout: EventEmitter<string> = new EventEmitter<string>();

    onLogin: EventEmitter<boolean> = new EventEmitter<boolean>();

    onSwitchPage: EventEmitter<string> = new EventEmitter<string>();

    loader: any;

    constructor(private loadingCtrl: LoadingController) {

    }

    loading(alertString: string = "Please wait...") {

        this.loader = this.loadingCtrl.create({
            content: alertString
            //duration: 3000
        });

        this.loader.present();

        return this.loader;
    }

    dismissLoading() {
        //console.log("Before IF");
        if(this.loader){
            //console.log("Inside IF");
            this.loader.dismissAll();
            this.loader = null;
        }

    }


}
