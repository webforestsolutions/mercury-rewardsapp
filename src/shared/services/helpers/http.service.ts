import {Injectable} from "@angular/core";
import {Headers, Http, RequestOptionsArgs, Response} from "@angular/http";
import "rxjs/Rx";
import {Observable} from "rxjs/Observable";

import {Constants} from '../../constants'
import {StorageService} from "./storage.service";

@Injectable()
export class HttpService {

    baseUrl: string = Constants.baseUrl;
    branchKey: string = "";

    constructor(private http: Http, private storageService: StorageService) {

    }

    public getWithoutKey(path: string, options?: RequestOptionsArgs): Observable<Response> {

        const requestUrl = this.getRequestUrl(path, true);
        const requestOptions = this.getOptions(options);

        console.log('HTTP GET WITHOUT KEY - ', requestUrl);

        return this.http.get(requestUrl, requestOptions);

    }

    public getFromPublicMiddleware(path: string, options?: RequestOptionsArgs): Observable<Response> {

        const requestUrl = this.getRequestUrl(path, false, true);
        const requestOptions = this.getOptions(options);

        console.log('HTTP GET FROM PUBLIC MIDDLEWARE - ', requestUrl);

        return this.http.get(requestUrl, requestOptions);

    }

    public get(path: string, options?: RequestOptionsArgs): Observable<Response> {

        let branchKeyObservable = this.getBranchKey();

        return branchKeyObservable.flatMap(
            (branchKey) => {

                console.log('branchKey - get: ', branchKey);

                this.branchKey = branchKey;

                const requestUrl = this.getRequestUrl(path);
                const requestOptions = this.getOptions(options);

                console.log('GET - ', requestUrl);

                return this.http.get(requestUrl, requestOptions);
            }
        );

    }

    public post(path: string, body: any, options?: RequestOptionsArgs): Observable<Response> {

        let branchKeyObservable = this.getBranchKey();

        return branchKeyObservable.flatMap(
            (branchKey) => {

                this.branchKey = branchKey;

                const requestUrl = this.getRequestUrl(path);
                const requestOptions = this.getOptions(options);

                console.log('POST - url: ', requestUrl);
                console.log('POST - body: ' + body);

                return this.http.post(requestUrl, body, requestOptions);
            }
        );


    }

    public postFromPublicMiddleware(path: string, body: any, options?: RequestOptionsArgs): Observable<Response> {

        const requestUrl = this.getRequestUrl(path, false, true);
        const requestOptions = this.getOptions(options);

        console.log('HTTP POST FROM PUBLIC MIDDLEWARE - url: ', requestUrl);
        console.log('POST - body: ', body);

        return this.http.post(requestUrl, body, requestOptions);


    }

    public put(path: string, body: any, options?: RequestOptionsArgs): Observable<Response> {

        let branchKeyObservable = this.getBranchKey();

        return branchKeyObservable.flatMap(
            (branchKey) => {

                this.branchKey = branchKey;

                const requestUrl = this.getRequestUrl(path);
                const requestOptions = this.getOptions(options);

                console.log('PUT - body: ', body);

                return this.http.put(requestUrl, body, requestOptions);
            }
        );

    }

    public destroy(path: string, options?: RequestOptionsArgs): Observable<Response> {

        let branchKeyObservable = this.getBranchKey();

        return branchKeyObservable.flatMap(
            (branchKey) => {

                this.branchKey = branchKey;

                const requestUrl = this.getRequestUrl(path);
                const requestOptions = this.getOptions(options);

                return this.http.delete(requestUrl, requestOptions);
            }
        );

    }

    public upload(path: string, body: any, options?: RequestOptionsArgs): Observable<Response> {

        let branchKeyObservable = this.getBranchKey();

        return branchKeyObservable.flatMap(
            (branchKey) => {

                this.branchKey = branchKey;

                const requestUrl = this.getRequestUrl(path);
                let requestOptions = this.getUploadOptions(options);

                console.log('UPLOAD|POST - body: ', body);

                return this.http.post(requestUrl, body, requestOptions);
            }
        );

    }

    public getBranchKey(): Observable<string> {

        return Observable.fromPromise(this.storageService.get(Constants.branchKey));

    }

    public getBranchRequestString() {

        // const branchKey = this.storageService.getItem(Constants.branchKey);
        const branchKey = this.branchKey;

        return "key=" + branchKey;

    }

    public getPublicRequestString() {

        return "sync_key=" + Constants.publicApiKey;

    }

    public getRequestUrl(path: string, excludeAdditionalRequestString: boolean = false, fromPublicMiddleware: boolean = false) {

        let requestUrl = this.baseUrl + path;

        if (excludeAdditionalRequestString) {
            return requestUrl;
        }

        const position = requestUrl.indexOf('?');

        if (fromPublicMiddleware && position == -1) {
            requestUrl += "?" + this.getPublicRequestString();
            return requestUrl;
        }

        if (fromPublicMiddleware && position >= 0) {
            requestUrl += "&" + this.getPublicRequestString();
            return requestUrl;
        }

        if (position == -1) {
            requestUrl += "?" + this.getBranchRequestString();
            return requestUrl;
        }

        requestUrl += "&" + this.getBranchRequestString();

        return requestUrl;
    }

    public getDefaultHeaders() {
        return new Headers({
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        });
    }

    public getUploadHeaders() {
        return new Headers({
            'Accept': 'application/json'
        });
    }

    private getOptions(options?: RequestOptionsArgs) {

        if (typeof options === 'undefined') {
            options = {headers: this.getDefaultHeaders()};
            return options;
        }

        if (typeof options.headers === 'undefined') {
            options.headers = this.getDefaultHeaders();
        }

        return options;
    }

    private getUploadOptions(options?: RequestOptionsArgs) {

        if (typeof options === 'undefined') {
            options = {headers: this.getUploadHeaders()};
            return options;
        }

        if (typeof options.headers === 'undefined') {
            options.headers = this.getUploadHeaders();
        }

        return options;
    }
}