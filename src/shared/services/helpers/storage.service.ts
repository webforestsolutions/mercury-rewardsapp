import {EventEmitter, Injectable} from "@angular/core";
import "rxjs/Rx";

import { Storage } from '@ionic/storage';

@Injectable()
export class StorageService {

    query: EventEmitter<string> = new EventEmitter<string>();

    constructor(private storage: Storage) { //
    }

    set(key:string, value:string){

        return this.storage.set(key, value);

    }

    get(key:string): Promise<any> {

        return this.storage.get(key);

    }

    setItem(key:string, value:string){
        return localStorage.setItem(key,value);
    }

    getItem(key:string){
        return localStorage.getItem(key);
    }

    remove(key:string){

        this.storage.remove(key);

    }



}