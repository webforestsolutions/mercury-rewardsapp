import {Injectable} from "@angular/core";
import "rxjs/Rx";
import {NgbModal, NgbModalOptions, NgbModalRef} from "@ng-bootstrap/ng-bootstrap";

import {AuthService} from '../api/auth.service';

@Injectable()
export class ModalService {

    constructor(private modal: NgbModal, private authService: AuthService) {

    }

    open(content:any, options?:NgbModalOptions): NgbModalRef {

        if(this.authService.isLoggedIn()){
            return this.modal.open(content, options);
        }

        /*return {
            result: new Promise((resolve, reject) => {
                reject();
            })
        }*/


    }


}