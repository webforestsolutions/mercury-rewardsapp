import {Injectable} from "@angular/core";
import {Response} from "@angular/http";
import "rxjs/Rx";

import {HttpService} from "../helpers/http.service";

@Injectable()
export class CategoryService {

    basePath: string = 'api/tracking/products/categories';
    transactionPath: string = 'api/tracking/transactions';

    constructor(private httpService: HttpService) {

    }

    getCategories() {

        const requestUrl = this.basePath;

        return this.httpService.get(requestUrl)
            .map(
                (response: Response) => {
                    return response.json().data;
                }
            )

    }

    getCategoriesByTransactionId(transactionId: number) {

        const requestUrl = this.transactionPath+'/'+transactionId+'/products/categories';

        return this.httpService.get(requestUrl)
            .map(
                (response: Response) => {
                    return response.json().data;
                }
            )

    }
}