import {Injectable} from "@angular/core";
import {Response} from "@angular/http";
import "rxjs/Rx";

import {Observable} from "rxjs/Observable";

import {HttpService} from "../helpers/http.service";
import {Transaction} from "../../../classes/transaction.class";
import {Constants} from "../../constants";
import {SingleAdjustment} from "../../../classes/single-adjustment.class";

@Injectable()
export class TransactionService {

    trackingPath: string = 'api/tracking/transactions';
    productsBasePath: string = 'api/products/transactions';
    adjustmentPath: string = 'api/tracking/transactions/products';

    transactionsPath: string = 'api/products/transactions';

    constructor(private httpService: HttpService) {

    }

    getTransactionHistory(key: string, subType?: string, limit: any = 'none', pageNumber?: number, query?: string): Observable<any> {

        const excludeVoid = 1;
        const group = Constants.getSaleGroup;

        if (typeof pageNumber == 'undefined') {
            pageNumber = 1;
        }

        if (typeof limit == 'undefined') {
            limit = 10;
        }

        if (typeof query == 'undefined') {
            query = "";
        }

        let requestUrl = this.trackingPath + "?page=" + pageNumber + "&limit=" + limit + "&q=" + query + "&exclude_void="
            + excludeVoid + "&group=" + group + "&tracking=1&sort=date&order=desc&range=daily&exclude_delivery=1&key=" + key;

        if (typeof subType !== 'undefined' && subType !== null && subType !== '') {
            requestUrl += "&sub_type=" + subType;
        }

        return this.httpService.get(requestUrl)
            .map(
                (response: Response) => {
                    return response.json();
                }
            )

    }

    getAllAdjustments(limit: any = 'none', pageNumber?: number, query?: string): Observable<any> {

        const excludeVoid = 1;
        const group = Constants.adjustmentGroup;

        if (typeof pageNumber == 'undefined') {
            pageNumber = 1;
        }

        if (typeof limit == 'undefined') {
            limit = 10;
        }

        if (typeof query == 'undefined') {
            query = "";
        }

        let requestUrl = this.trackingPath + "?page=" + pageNumber + "&limit=" + limit + "&q=" + query + "&exclude_void=" + excludeVoid + "&group=" + group + "&tracking=1&range=monthly";

        return this.httpService.get(requestUrl)
            .map(
                (response: Response) => {
                    console.log('response: ', response.json());
                    return response.json();
                }
            )

    }

    findTransactionByOr(orNumber: string): Observable<any> {

        let requestUrl = this.trackingPath + "/receipts/" + orNumber;

        return this.httpService.get(requestUrl)
            .map(
                (response: Response) => {
                    return response.json();
                }
            )
    }

    findTransactionById(transactionId: number): Observable<any> {

        let requestUrl = this.trackingPath + "/" + transactionId;

        return this.httpService.get(requestUrl)
            .map(
                (response: Response) => {
                    return response.json();
                }
            )
    }

    addSaleTransaction(transaction: Transaction) {

        const body = JSON.stringify(transaction);

        const requestUrl = this.trackingPath + '/sale';

        return this.httpService.post(requestUrl, body)
            .map(
                (response: Response) => response.json().data
            );
    }

    returnSaleTransaction(transaction: Transaction) {

        const body = JSON.stringify(transaction);

        const requestUrl = this.trackingPath + '/sale/return';

        return this.httpService.post(requestUrl, body)
            .map(
                (response: Response) => response.json().data
            );
    }

    voidSaleTransaction(transactionId: number, staffId: number) {

        let body = {
            staff_id: staffId
        };

        const requestUrl = this.trackingPath + '/' + transactionId + '/void';

        return this.httpService.post(requestUrl, body)
            .map(
                (response: Response) => response.json().data
            );
    }

    voidReturnSaleTransaction(transactionId: number, staffId: number) {

        let body = {
            staff_id: staffId
        };

        const requestUrl = this.trackingPath + '/sale/return/' + transactionId + '/void';

        return this.httpService.post(requestUrl, body)
            .map(
                (response: Response) => response.json().data
            );
    }

    createSingleAdjustment(adjustment: SingleAdjustment) {

        let transaction = new Transaction();
        transaction.or_no = adjustment.or_no;
        transaction.staff_id = adjustment.staff_id;
        transaction.remarks = adjustment.getRemarksByCode(adjustment.remarks_code);

        adjustment.type = Constants.shortCode;

        transaction.setItem(adjustment.product_variation_id, adjustment.quantity);

        if (adjustment.remarks_code == Constants.shortOverCode) {
            transaction.setShortOverItem(adjustment.product_variation_id, adjustment.shortover_product_variation_id, adjustment.quantity);
            return this.addShortOverTransaction(transaction);
        }

        return this.addShortTransaction(transaction);


    }

    addShortTransaction(transaction: Transaction) {

        const body = JSON.stringify(transaction);

        const requestUrl = this.adjustmentPath + '/short';

        return this.httpService.post(requestUrl, body)
            .map(
                (response: Response) => response.json().data
            );

    }

    addShortOverTransaction(transaction: Transaction) {

        const body = JSON.stringify(transaction);

        const requestUrl = this.adjustmentPath + '/shortover';

        return this.httpService.post(requestUrl, body)
            .map(
                (response: Response) => response.json().data
            );

    }

    calculateDiscount(transaction: Transaction) {

        const body = JSON.stringify(transaction);

        const requestUrl = this.trackingPath + '/discounts/calculate';

        return this.httpService.post(requestUrl, body)
            .map(
                (response: Response) => response.json()
            );
    }

    getBranchesSalesSummary(range: string = 'day', subType: string = null, keys: string = null): Observable<any> {

        if (typeof range == 'undefined' || range == null) {
            range = 'day';
        }

        let requestUrl = this.transactionsPath + "/sales?range=" + range;

        if (typeof subType !== 'undefined' && subType !== null && subType !== "") {
            requestUrl += "&sub_type=" + subType;
        }

        if (typeof keys !== 'undefined' && keys !== null && keys !== "") {
            requestUrl += "&keys=" + keys;
        }

        return this.httpService.get(requestUrl)
            .map(
                (response: Response) => {
                    console.log('response: ', response.json());
                    return response.json();
                }
            )

    }

    getSalesSummary(branchId: number, range: string = 'day', subType: string = null): Observable<any> {

        if (typeof range == 'undefined' || range == null) {
            range = 'day';
        }

        let requestUrl = this.transactionsPath + "/sales/summary?range=" + range;

        if (typeof branchId !== 'undefined' && branchId !== null && !isNaN(branchId)) {
            requestUrl += "&branch_id=" + branchId;
        }

        if (typeof subType !== 'undefined' && subType !== null && subType !== '') {
            requestUrl += "&sub_type=" + subType;
        }

        return this.httpService.get(requestUrl)
            .map(
                (response: Response) => {
                    console.log('response: ', response.json());
                    return response.json();
                }
            )

    }

    getSalesChanges(range: string = 'day', branchId: number = null, subType: string = null): Observable<any> {

        if (typeof range == 'undefined' || range == null) {
            range = 'day';
        }

        let requestUrl = this.transactionsPath + "/sales/summary/changes?range=" + range;

        if (typeof branchId !== 'undefined' && branchId !== null && !isNaN(branchId)) {
            requestUrl += "&branch_id=" + branchId;
        }

        if (typeof subType !== 'undefined' && subType !== null && subType !== '') {
            requestUrl += "&sub_type=" + subType;
        }

        return this.httpService.get(requestUrl)
            .map(
                (response: Response) => {
                    console.log('response: ', response.json());
                    return response.json();
                }
            )

    }

    getTopSaleItems(range: string = 'day', branchId?: number, subType?: string, limit: number = 7): Observable<any> {

        let requestUrl = this.productsBasePath + '/sales/top?version=1';

        if (typeof range !== 'undefined' && range != null) {
            requestUrl += "&range=" + range;
        }

        if (typeof branchId !== 'undefined' && branchId !== null && !isNaN(branchId)) {
            requestUrl += "&branch_id=" + branchId;
        }

        if (typeof subType !== 'undefined' && subType !== null && subType !== '') {
            requestUrl += "&sub_type=" + subType;
        }

        if (typeof limit !== 'undefined' && limit !== null && !isNaN(limit)) {
            requestUrl += "&limit=" + limit;
        }

        return this.httpService.get(requestUrl)
            .map(
                (response: Response) => {
                    console.log('response: ', response.json());
                    return response.json();
                }
            )
    }

    voidTransaction(transactionId: number) {

        let body = {};

        const requestUrl = this.productsBasePath + '/' + transactionId + '/void';

        return this.httpService.post(requestUrl, body)
            .map(
                (response: Response) => response.json().data
            );
    }

}