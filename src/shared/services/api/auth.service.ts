import {EventEmitter, Injectable} from "@angular/core";
import {Headers, Http, Response} from "@angular/http";
import "rxjs/Rx";
import {Observable} from "rxjs/Observable";

import {Constants} from "../../constants";
import {StorageService} from "../helpers/storage.service";

@Injectable()
export class AuthService {

    basePath: string = 'api/auth';
    baseUrl = Constants.baseUrl + this.basePath;

    constructor(
        private http: Http,
        private storageService: StorageService
    ) {}

    getMemberId(): Promise<any> {
        return this.storageService.get(Constants.memberId);
    }

    isLoggedIn() {

        const token = this.getToken();

        if (typeof token === 'undefined' || token === null) {
            return false;
        }

        const decodedTokenData = this.decodeToken(token);

        if (typeof decodedTokenData.exp == 'undefined') {
            return false;
        }

        const expTimestamp = decodedTokenData.exp * 1000;
        // const refreshTimestamp = decodedTokenData.nbf*1000;
        const todaysTimestamp = new Date().getTime();

        if (expTimestamp < todaysTimestamp) {
            this.removeToken();
            return false;
        }

        return true;
    }

    removeToken() {
        localStorage.removeItem('token');
    }

    getToken() {
        return localStorage.getItem('token');
    }

    decodeToken(token) {
        const base64Url = token.split('.')[1];
        const base64 = base64Url.replace('-', '+').replace('_', '/');
        return JSON.parse(window.atob(base64));
    }
}
