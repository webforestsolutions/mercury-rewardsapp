import {Injectable} from "@angular/core";
import {Response} from "@angular/http";
import "rxjs/Rx";

import {HttpService} from "../helpers/http.service";
import {User} from '../../../classes';
import {Observable} from "rxjs/Observable";

@Injectable()
export class RewardService {

    basePath: string = 'api/tracking/sync/rewards';

    constructor(private httpService: HttpService) {
    }

    getRewardsByMemberId(memberId: string): Observable<any> {

        // GET - api/tracking/sync/rewards/members/${memberId}
        const requestUrl = this.basePath + '/members/' + memberId;

        return this.httpService.getFromPublicMiddleware(requestUrl)
            .map(
                (response: Response) => {
                    return response.json().data;
                }
            );
    }
}
