import {Injectable} from "@angular/core";
import {Response} from "@angular/http";
import "rxjs/Rx";

import {
    HttpService, StorageService
} from "../helpers";

import {User} from '../../../classes';
import {Constants} from '../../constants';
import {Observable} from "rxjs/Observable";

@Injectable()
export class UserService {

    basePath: string = 'api/tracking/users';

    constructor(private httpService: HttpService, private storageService: StorageService) {}

    getCurrentCustomerId(): Promise<any> {
        return this.storageService.get(Constants.memberId)
            .then(
                (response) => {
                    return response;
                }
            )
            .catch(
                (error) => {
                    console.log('Something went wrong while trying to retrieve the current user - ', error)
                }
            );
    }


}
