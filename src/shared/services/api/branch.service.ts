import {Injectable} from "@angular/core";
import {Response} from "@angular/http";
import "rxjs/Rx";
import {Observable} from "rxjs/Observable";
import {HttpService} from "../helpers/http.service";

@Injectable()
export class BranchService {

    basePath: string = 'api/branches';

    constructor(private httpService: HttpService) {

    }

    getBranches(): Observable<any> {

        let requestUrl = this.basePath;

        return this.httpService.get(requestUrl)
            .map(
                (response: Response) => {
                    return response.json();
                }
            )
    }

    getBranchesByKeys(branchKeysString: string = ""): Observable<any> {

        let requestUrl = this.basePath + '?keys=' + branchKeysString;

        return this.httpService.get(requestUrl)
            .map(
                (response: Response) => {
                    return response.json();
                }
            )

    }

    getBranchByKey(branchKey: any): Observable<any> {

        let requestUrl = this.basePath + '/key/' + branchKey;

        return this.httpService.get(requestUrl)
            .map(
                (response: Response) => {
                    return response.json();
                }
            )

    }
}