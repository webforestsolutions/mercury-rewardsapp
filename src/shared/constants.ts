import {ENV} from "@app/env";

export class Constants {

    static publicApiKey: string = "public_sync";
    static memberId: string = "member_id";

    public static get baseUrl(): string {

        console.log("ENVIRONMENT: ", ENV);

        if(ENV.url == '' || typeof ENV.url == 'undefined' || ENV.url == null){
            return 'http://ecasanes.xyz';
        }

        return ENV.url;
    };

    public static get defaultMetrics(): string {
        return "ml";
    };

    public static get getMetrics(): {name:string, value:string}[]{
        return [
            {
                name: "Milliliters",
                value: "ml"
            },
            {
                name: "Liters",
                value: "l"
            }
        ];
    }

    public static get getConfirmedStatus(): string {
        return 'confirmed';
    }

    public static get adminRole(): string {
        return 'admin';
    }

    public static get companyRole(): string {
        return 'company';
    }

    public static get companyStaffRole(): string {
        return 'company_staff';
    }

    public static get staffRole(): string {
        return 'company_staff';
    }

    public static get getInventoryPermission(): string {
        return 'inventory';
    }

    public static get getSalesPermission(): string {
        return 'sales';
    }

    public static get getPendingStatus(): string {
        return 'pending';
    }

    public static get getInventoryGroup(): string {
        return 'inventory';
    }

    public static get getSaleGroup(): string {
        return 'sale';
    }

    public static get saleTransactionType(): string {
        return 'sale';
    }

    public static get getNone(): string {
        return 'none';
    }

    public static get getVoidSaleCode(): string {
        return 'void_sale';
    }

    public static get branchKey(): string {
        return 'branchKey';
    }

    public static get branchKeys(): string {
        return 'branchKeys';
    }

    public static get currentRole(): string {
        return 'current_role';
    }

    public static get assignedBranches(): string {
        return 'branches';
    }

    public static get memberRole(): string {
        return 'member';
    }

    public static get adjustmentTypes(): {remarks:string, code:string}[] {
        return [
            {
                remarks: "Short",
                code: "short"
            },
            {
                remarks: "Lost",
                code: "lost"
            },
            {
                remarks: "Damage",
                code: "damage"
            },
            {
                remarks: "Short/Over",
                code: "shortover"
            }
        ];
    }

    public static get shortOverCode(): string {
        return 'shortover';
    }

    public static get shortOverAdjustmentCode(): string {
        return 'adjustment_shortover';
    }

    public static get shortCode(): string {
        return 'short';
    }

    public static get shortAdjustmentCode(): string {
        return 'adjustment_short';
    }

    public static get adjustmentGroup(): string {
        return 'adjustment';
    }

    public static get returnSaleCode(): string {
        return 'return_sale';
    }

    public static get saleCode(): string {
        return 'sale';
    }

    public static get activeFlag(): string {
        return 'active';
    }

    public static get franchiseeFlag(): string {
        return 'franchisee';
    }

    public static get coordinatorFlag(): string {
        return 'coordinator';
    }

    public static get ownerFlag(): string {
        return 'owner';
    }

    public static get confirmVoidTransaction(): string {
        return 'Are you sure you want to void this transaction?';
    }

    public static get voidSuccessMessage(): string {
        return 'Transaction has been voided';
    }

    public static get voidErrorMessage(): string {
        return 'Transaction could not be voided';
    }

    public static get noBranchesAdded(): string {
        return 'We did not find any branches on your list. Proceed to add branch page?';
    }

    public static get successCode(): string {
        return 'success';
    }

    public static get errorCode(): string {
        return 'error';
    }

    public static get email(): string {
        return 'email';
    }

}