import {BrowserModule} from '@angular/platform-browser';
import {ErrorHandler, NgModule} from '@angular/core';
import {IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';
import {SplashScreen} from '@ionic-native/splash-screen';
import {StatusBar} from '@ionic-native/status-bar';

import {MyApp} from './app.component';
import {TruncateModule} from "ng2-truncate";
import {ToastrModule} from "ngx-toastr";
import {HttpModule} from "@angular/http";
import {FormsModule} from "@angular/forms";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {BarcodeScanner} from '@ionic-native/barcode-scanner';

import {
    HttpService,
    DashboardEventService,
    ProductService,
    AuthService,
    CategoryService,
    BranchService,
    RoleService,
    DeliveryService,
    ErrorResponseService,
    SearchService,
    UserService,
    RewardService,
    AlertService,
    TransactionService,
    PricingService,
    StorageService
} from "../shared";

import { IonicStorageModule } from '@ionic/storage';

import {
    AlertErrorHandlerComponent,
    ErrorHandlerComponent
} from "../components/directives";


// IN CASE MY METHOD DOES NOT WORK
import {TrackingEventService} from "../shared/services/helpers/tracking-event.service";
import {PlatformService} from "../shared/services/helpers/platform.service";

// SERVICES
// import {
//     TrackingEventService,
//     PlatformService
// } from '../shared/services';

// PAGES
import {
    SigninPage,
    MemberIdLoginPage,
    HomePage,
    ProductsPage,
    SingleProductPage,
    PromosPage
} from '../pages';

@NgModule({
    declarations: [
        MyApp,
        SigninPage,
        MemberIdLoginPage,
        HomePage,
        ProductsPage,
        SingleProductPage,
        PromosPage,
        AlertErrorHandlerComponent,
        ErrorHandlerComponent,
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        IonicModule.forRoot(MyApp, {
            animate: false
        }),
        IonicStorageModule.forRoot(),
        TruncateModule,
        HttpModule,
        FormsModule,
        ToastrModule.forRoot({
            timeOut: 5000,
            positionClass: 'toast-bottom-right',
            preventDuplicates: true
        }),

    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
        SigninPage,
        MemberIdLoginPage,
        HomePage,
        ProductsPage,
        SingleProductPage,
        PromosPage
    ],
    providers: [
        StatusBar,
        SplashScreen,
        {provide: ErrorHandler, useClass: IonicErrorHandler},
        StorageService,
        HttpService,
        DashboardEventService,
        TrackingEventService,
        ProductService,
        AuthService,
        CategoryService,
        BranchService,
        RoleService,
        DeliveryService,
        ErrorResponseService,
        SearchService,
        UserService,
        RewardService,
        AlertService,
        TransactionService,
        PricingService,
        PlatformService,
        BarcodeScanner
    ]
})
export class AppModule {
}
