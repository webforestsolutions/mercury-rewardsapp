import {Component, ViewChild} from '@angular/core';
import {AlertController, Nav, Platform} from 'ionic-angular';
import {AuthService} from "../shared/services/api/auth.service";
import {SplashScreen} from '@ionic-native/splash-screen';
import {StorageService} from "../shared/services/helpers/storage.service";
import {BranchService} from "../shared/services/api/branch.service";
import {TrackingEventService} from "../shared/services/helpers/tracking-event.service";
import {Page} from "../classes/page.class";
import {Constants} from "../shared/constants";

// PAGES
import {
    HomePage,
    SigninPage,
    ProductsPage,
    PromosPage
} from "../pages";

@Component({
    templateUrl: 'app.html'
})
export class MyApp {

    @ViewChild(Nav) nav: Nav;

    rootPage: any;

    branches: any = [];
    pages: Page[] = [];

    loggedIn: boolean = false;

    selected: string = 'account';

    logoutAlertShown: boolean = false;

    constructor(public platform: Platform,
                private authService: AuthService,
                public splashScreen: SplashScreen,
                private alertCtrl: AlertController,
                private storageService: StorageService,
                private branchService: BranchService,
                private trackingEventService: TrackingEventService) {

        this.initializeApp();
        this.initializeSubscriptions();

        // let backAction = platform.registerBackButtonAction(
        //     () => {

        //         if (this.loggedIn) {
        //             console.log('LOGOUT');
        //             return this.trackingEventService.onLogout.emit('logout');
        //         }

        //         console.log('POP');
        //         return this.navCtrl.pop();

        //     }
        // );
    }

    initializeApp() {
        this.platform.ready().then(() => {
            this.checkSession();
        });
    }

    initializeSubscriptions() {

        this.trackingEventService.onMemberVerified.subscribe(
            (memberId) => {
                this.loggedIn = true;
                this.rootPage = HomePage;
                console.log('LOGIN STATUS - ', this.loggedIn);
            }
        );

        this.trackingEventService.onLogout.subscribe(
            (response) => {
                this.loggedIn = false;
                this.rootPage = SigninPage;
                this.removeCurrentMemberId();
            }
        );

        this.trackingEventService.onSwitchPage.subscribe(
            (response) => {
                this.selected = response;
            }
        );

    }

    checkSession() {

        this.trackingEventService.loading("Loading, Please wait...");

        this.authService.getMemberId().then(
            (memberId) => {

                console.log("MEMBER ID: ", memberId);

                this.trackingEventService.dismissLoading();

                if(!memberId || memberId.trim() == "" || !this.loggedIn){
                    this.splashScreen.hide();
                    this.rootPage = SigninPage;
                    return;
                }

                this.splashScreen.hide();
                this.rootPage = HomePage;
                return;

            });
    }

    switchPage(page: string): void {

        this.selected = page;

        switch(page) {
            case 'account':
                this.onClickHomePage();
                break;
            case 'products':
                this.onClickProductsPage();
                break;
            case 'promo':
                this.onClickPromoPage();
                break;
            default:
                this.onClickHomePage();
                break;
        }

    }

    // NAVIGATION

    onClickHomePage() { // AKA ACCOUNTS ON THE FOOTER NAV
        this.nav.push(HomePage);
    }

    onClickProductsPage() {
        this.nav.push(ProductsPage);
    }

    onClickPromoPage() {
        this.nav.push(PromosPage);
    }

    // LOGOUT

    onLogout() {

        let alert = this.alertCtrl.create({
            title: 'Confirm Logout',
            message: 'Are you sure you want to logout?',
            buttons: [
                {
                    text: 'No',
                    role: 'Cancel',
                    cssClass: 'cancel',
                    handler: () => {
                        console.log('Clicked cancel');
                    }
                },
                {
                    text: 'Yes',
                    cssClass: 'confirm',
                    handler: () => {
                        this.trackingEventService.onLogin.emit(false);
                        this.trackingEventService.onLogout.emit('logout');
                        this.removeCurrentMemberId();
                    }
                }
            ]
        });

        alert.present().then(
            () => {
                console.log('Redirected to login page');
            }
        );
    }

    private removeCurrentMemberId(): void {
        this.storageService.set(Constants.memberId, '');
    }

}
